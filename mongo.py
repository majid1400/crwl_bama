from pymongo import MongoClient

client = MongoClient()

# Database
db = client['bama']

# Collection
infoSamand = db.links


# insert data
def save_links(data):
    return infoSamand.insert_many(data)
