from bs4 import BeautifulSoup


def extract_li(li):
    result = {
        "title": None,
        "time": None,
        "price": None,
        "link": None,
    }

    title = li.find('h2')
    if title is not None:
        listTitle = []
        for i in title.text.strip().split('،'):
            listTitle.append(i.strip())
        result['title'] = (' ').join(listTitle)

    time = li.find('p', {'class': 'mod-date-car-page'})
    if time is not None:
        result['time'] = time.text

    price = li.find('span', {'itemprop': 'price'})
    if price:
        result['price'] = price.attrs.get('content', '')

    a = li.find('a')
    if a is not None:
        link = a.attrs.get('href', '')
        news = link.split('/')[3]
        if news != 'news':
            result['link'] = link

    if result:
        return result


def parse_html(data):
    soup = BeautifulSoup(data)
    adv_div = soup.select_one('#adlist')

    adv_list = adv_div.find_all('li')
    list_result = []
    for li in adv_list:
        ads = li.attrs.get('class', None)
        if not ads:
            a = li.find('a')
            if a is not None:
                link = a.attrs.get('href', '')
                news = link.split('/')[3]
                if news != 'news':
                    list_result.append(extract_li(li))
    print(len(list_result))
    return list_result
