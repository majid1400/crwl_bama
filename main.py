import requests

from store import store_data


def crawl_site(url):
    crawl = True
    page = 0

    while crawl:
        page += 1
        response = requests.get(url.format(page))
        crawl = bool(response.url == url.format(page))

        if crawl and response.ok:
            store_data(response)

    print("#### {} page Found".format(page - 1))


if __name__ == "__main__":
    crawl_site("https://bama.ir/car/samand/lx/ef7?page={}")
