from urllib.parse import urlparse

from mongo import save_links
from parseer import parse_html


def generate_filename(url):
    p = urlparse(url)
    name = ("_").join(p.path.split('/')[2:]) \
           + '_' + p.query.replace('=', '_') + '.html'
    return name


def store_data_to_file(response):
    with open(generate_filename(response.url), 'w', encoding='utf-8') as f:
        f.write(response.text)


def store_data_to_mongo(response):
    data = parse_html(response.text)
    save_links(data)
    return len(data)


def store_data(response, db=True):
    print(response.url)
    if db:
        return store_data_to_mongo(response)
    return store_data_to_file(response)
